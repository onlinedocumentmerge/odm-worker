/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.JMSService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.odmworker.Entity.ODMJobs;
import com.odmworker.Service.IJobService;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Slf4j
@Service
public class JMSJobService {
    //Constant
    private static final String SEND_JOB = "Workerjob.queue";
    private static final String SEND_TO_DELETE_JOB = "ODMjobDelete.queue";
    private static final String SEND_TO_RESUME = "ODMjobResume.queue";
    private static final String RECEIVE_TO_EXECUTE_JOB = "ODMjobExecute.queue";
    private static final String RECEIVE_TO_PAUSE = "WorkerjobPause.queue";
    private static final String RECEIVE_TO_DELETE = "WorkerjobDelete.queue";
    private static final String RECEIVE_TO_RESUME = "WorkerjobResume.queue";
    private static final String RECEIVE_TO_SCHEDULE_JOB = "WorkerjobSchedule.queue";
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private IJobService jobService;

    public void sendStatusMessage(ODMJobs odmjob, String mgsType) {
        MessageCreator messageCreator = new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                ObjectMapper mapper = new ObjectMapper();
                ObjectMessage objectMessage = session.createObjectMessage();
                try {
                    String json = mapper.writeValueAsString(odmjob);
                    objectMessage.setObject(json);
                    return objectMessage;
                } catch (JsonProcessingException ex) {
                    log.info(ex.getMessage());
                    return null;
                }
            }
        };
        if (messageCreator != null) {
            jmsTemplate.send(mgsType, messageCreator);
            log.info("Send message success!");
        } else {
            log.error("Cannot connect to ODM, please contact administrator");
        }
    }

    @JmsListener(destination = RECEIVE_TO_EXECUTE_JOB)
    public void receiveToExecuteJobMessage(String jsonString) {

        try {
            Gson g = new Gson();
            ODMJobs jobs = g.fromJson(jsonString, ODMJobs.class);
            if (jobService.findByNameAndGroup(jobs.getName(), jobs.getJobGroup()) != null) {
                jobService.startJobNow(jobs);
            } else {
                jobs.setInQueue(true);
                jobs.setStart(true);
            }

            //jobInfo.setCronJob(false);
            //jobInfo.setRepeatTime(5L);
            jobService.startJobNow(jobs);
            log.info("Added new Job: " + jobs.getName() + " success!");
            sendStatusMessage(jobs, SEND_JOB);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @JmsListener(destination = RECEIVE_TO_SCHEDULE_JOB)
    public void scheduleJobMessage(String jsonString) {
        try {
            Gson g = new Gson();
            ODMJobs jobs = g.fromJson(jsonString, ODMJobs.class);
            jobs.setInQueue(true);
            jobs.setStart(true);
            log.info("scheduling Job: "+jobs.getName());
            jobService.scheduleNewJob(jobs);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @JmsListener(destination = RECEIVE_TO_PAUSE)
    public void pauseJobMessage(String jsonString) {
        try {
            Gson g = new Gson();
            ODMJobs jobs = g.fromJson(jsonString, ODMJobs.class);
            ODMJobs runningJob = jobService.findByNameAndGroup(jobs.getName(), jobs.getJobGroup());

            jobService.pauseJob(runningJob);
            runningJob.setStart(false);
            sendStatusMessage(runningJob, SEND_JOB);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @JmsListener(destination = RECEIVE_TO_DELETE)
    public void deleteJobMessage(String jsonString) {
        try {
            Gson g = new Gson();
            ODMJobs jobs = g.fromJson(jsonString, ODMJobs.class);
            ODMJobs existedJob = jobService.findByNameAndGroup(jobs.getName(), jobs.getJobGroup());

            jobService.deleteJob(existedJob);
            sendStatusMessage(existedJob, SEND_TO_DELETE_JOB);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @JmsListener(destination = RECEIVE_TO_RESUME)
    public void resumeJobMessage(String jsonString) {
        try {
            Gson g = new Gson();
            ODMJobs jobs = g.fromJson(jsonString, ODMJobs.class);
            ODMJobs existedJob = jobService.findByNameAndGroup(jobs.getName(), jobs.getJobGroup());

            jobService.resumeJob(existedJob);
            sendStatusMessage(existedJob, SEND_TO_RESUME);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
   

}
