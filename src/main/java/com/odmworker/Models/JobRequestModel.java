/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Models;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ADMIN
 */
@Getter
@Setter
public class JobRequestModel {
    private String name;
    private String group;
    private Boolean cronJob;
    private String cronExpression;
    private Long repeatTime;
    
    
}
