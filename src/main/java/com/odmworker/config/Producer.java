package com.odmworker.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odmworker.Entity.ODMJobs;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

@Component
@Slf4j
public class Producer {
    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(ODMJobs odmJobs)
	{ 
    	String queueName = "outbound.queue";
		jmsTemplate.send(queueName, new MessageCreator()
		{
			
				@Override
				public Message createMessage (Session session) throws JMSException {
					ObjectMapper mapper  = new ObjectMapper();
					ObjectMessage objectMessage = session.createObjectMessage();
					
				try {
					String json = mapper.writeValueAsString(odmJobs);
					objectMessage.setObject(json);
					
				} catch (JsonProcessingException ex) {
					Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
					
				}
					return objectMessage;
					
				}
			
		});
	}
}
