/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Controller;

import com.google.gson.Gson;
import com.google.inject.spi.Message;
import com.odmworker.Entity.ODMJobs;
import com.odmworker.Models.JobRequestModel;
import com.odmworker.Service.IJobService;
import com.odmworker.Utils.CronExxpressionMaker;
import com.odmworker.config.Producer;
import io.swagger.annotations.ApiOperation;

import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Optional;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
//import javax.jms.Session;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author ADMIN
 */
@Slf4j
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/WorkerController")
public class WorkerController {

    @Autowired
    private CronExxpressionMaker cronExxpressionMaker;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private ApplicationContext context;
    @Autowired
    private IJobService jobService;
    @Autowired
    JmsTemplate jmsTemplate;
    @Autowired
    ActiveMQConnectionFactory activeMqConnection;
    

    @ApiOperation(value = "")
    
    @PostMapping("/job/create")
    public ResponseEntity createNewJob(@RequestBody JobRequestModel jobRequest) {
        //JobDetail jobDetail = jobScheduleCreator.createJob(JobFind.class, true,context, "test","groupTest");
        try {
            ODMJobs jobInfo = modelMapper.map(jobRequest, ODMJobs.class);
            jobService.scheduleNewJob(jobInfo);
            return ResponseEntity.ok("success!");

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(value = "Can only be executed if job are not started")
    @PostMapping("/job/executeAll")
    public ResponseEntity executeAll() {
        try {
            jobService.startAllSchedulers();
            return ResponseEntity.ok("success!");
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/job/unschedule/{jobName}")
    public ResponseEntity unScheduleJob(@PathVariable String jobName) {
        try {
            jobService.unScheduleJob(jobName);
            return ResponseEntity.ok("success!");
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    

    @ApiOperation(value = "Pause job")
    @PostMapping("/job/pause")
    public ResponseEntity pauseJob(@RequestBody JobRequestModel jobRequest) {
        try {
            ODMJobs jobInfo = modelMapper.map(jobRequest, ODMJobs.class);
            jobService.pauseJob(jobInfo);
            return ResponseEntity.ok("success!");

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "work only with job are paused")
    @PostMapping("/job/resume")
    public ResponseEntity resumeJob(@RequestBody JobRequestModel jobRequest) {
        try {
            ODMJobs jobInfo = modelMapper.map(jobRequest, ODMJobs.class);
            jobService.resumeJob(jobInfo);
            return ResponseEntity.ok("success!");

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("cron/test")
    public ResponseEntity cronMaker() {
        try {
            
            return ResponseEntity.ok(cronExxpressionMaker.CronMaker());

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/job/delete")
    public ResponseEntity deleteJob(@RequestBody JobRequestModel jobRequest) {
        try {
            ODMJobs jobInfo = modelMapper.map(jobRequest, ODMJobs.class);
            jobService.deleteJob(jobInfo);
            return ResponseEntity.ok("success!");

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/job/deleteAll")
    public ResponseEntity deleteAllJob() {
        try {
           
            jobService.deleteAllJob();
            return ResponseEntity.ok("success!");

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/job/{jobId}")
    public ResponseEntity getJob(@PathVariable int jobId) {
        try {

            return ResponseEntity.ok(jobService.getJobById(jobId).getName());

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/job/start")
    public ResponseEntity startJob(@RequestBody ODMJobs jobRequest) {
        try {
            //ODMJobs jobInfo = modelMapper.map(jobRequest, ODMJobs.class);
            jobService.startJobNow(jobRequest);
            return ResponseEntity.ok("success!");

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
    @GetMapping("/getMsg")
    public void receive() throws JMSException {
//    	Connection connection = activeMqConnection.createConnection("admin", "admin");
//    	connection.start();
//    		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//    		Queue queue = session.createQueue("test.queue");
//    		QueueBrowser queueBrowser = session.createBrowser(queue);
//    		Enumeration msgs = queueBrowser.getEnumeration();
//    		queueBrowser.getMessageSelector();
////     
//    		if ( !msgs.hasMoreElements() ) { 
//    		    System.out.println("No messages in queue");
//    		} else { 
//    			
//    		  System.out.println("co msg");
//    		  String msg = null;
//    		  javax.jms.Message message = jmsTemplate.receive("test.queue");
//    		  message = session.createTextMessage(msg);
//    		  System.out.println( message);
    		  
    	
//    }
    	String messageSelector;
    	int size = jmsTemplate.browseSelected("test1.queue", null,new BrowserCallback<Integer>() {

			@Override
			public Integer doInJms(Session session, QueueBrowser browser) throws JMSException {
				return Collections.list(browser.getEnumeration()).size();
			}
    		
    	});
    	System.out.println(size);
    		  javax.jms.Message message = jmsTemplate.receive("test1.queue");	
    }
}
