/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Service;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author ADMIN
 */
public interface IDocumentService {
     String mergeDocument(MultipartFile template, MultipartFile dataSource);
}
