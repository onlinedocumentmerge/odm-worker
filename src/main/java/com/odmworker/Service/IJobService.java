/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Service;

import com.odmworker.Entity.ODMJobs;
import java.util.Date;
import java.util.List;
import org.quartz.SchedulerException;

/**
 *
 * @author ADMIN
 */
public interface IJobService {

    List<ODMJobs> getODMJob();

    void setQueueJob();

    ODMJobs getJobById(int id);

    void createNewJob(ODMJobs jobInfo);

    void startAllSchedulers();

    void scheduleNewJob(ODMJobs jobInfo) throws Exception;

    void updateScheduleJob(ODMJobs jobInfo);

    boolean unScheduleJob(String jobName);

    boolean deleteJob(ODMJobs jobInfo);

    boolean pauseJob(ODMJobs jobInfo);

    boolean resumeJob(ODMJobs jobInfo);

    void startJobNow(ODMJobs jobInfo)throws Exception;

    void deleteAllJob() throws SchedulerException;
    
    

    ODMJobs findByNameAndGroup(String name, String jobGroup)throws Exception;
}
