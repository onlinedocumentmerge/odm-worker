/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Service.ServiceImpl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odmworker.Service.IDocumentService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author ADMIN
 */
@Service
public class DocumentServiceImpl implements IDocumentService{
    @Override
    public String mergeDocument(MultipartFile template, MultipartFile dataSource) {
        List<String> documents = new ArrayList<>();

        try {
            //read template
            String fileDir = getDataDir(DocumentServiceImpl.class);
            File fileTemp = new File(fileDir);
//            File fileTemp = new File("C:\\Users\\84918\\Desktop\\merge\\template.html");
            template.transferTo(fileTemp);
            String templateStr = convertFileToString(fileTemp);

            //convert string to jsonObject
            File fileDataSource = new File(fileDir);
//            File fileDataSource = new File("C:\\Users\\84918\\Desktop\\merge\\datasource.json");
            dataSource.transferTo(fileDataSource);
            String dataSourceStr = convertFileToString(fileDataSource);
            //convert json string to json tree
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(dataSourceStr);
            System.out.println(dataSourceStr);
            String lastNode = dataSourceStr.substring(dataSourceStr.lastIndexOf("{") , dataSourceStr.lastIndexOf("}") + 1);
//            if(jsonNode.isArray()) {
            //get key from 1 node
            JSONObject node1 = new JSONObject(lastNode);
            Set<String> keys = node1.keySet();

            for (JsonNode jsonNode1 : jsonNode) {
                String document = templateStr.substring(0, templateStr.length() - 1);
                for (String key : keys) {
                    //get value in key
                    String value = jsonNode1.get("" + key + "").asText();
                    //replace all key in template by value
                    document = document.replaceAll("<ODMNode>" + key + "</ODMNode>", value);
                }
                documents.add(document);
            }
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (documents.isEmpty()) {
            return "";
        }
        return documents.get(0);
    }

    public static String convertFileToString(File file) throws IOException {
        String content = "";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            byte[] buffer = new byte[10];
            StringBuilder sb = new StringBuilder();
            while (fis.read(buffer) != -1) {
                sb.append(new String(buffer));
                buffer = new byte[10];
            }

            content = sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
        }

        return content;
    }

    public static String getDataDir(Class c) {
        File dir = new File(System.getProperty("user.dir"));
        dir = new File(dir, "src");
        dir = new File(dir, "main");
        dir = new File(dir, "resources");

        for (String s : c.getName().split("\\.")) {
            dir = new File(dir, s);
            if (dir.isDirectory() == false) {
                dir.mkdir();
            }
        }
        System.out.println("Using data directory: " + dir.toString());
        return dir.toString() + File.separator;
    }
}
