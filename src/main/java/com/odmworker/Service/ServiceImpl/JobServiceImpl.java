/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Service.ServiceImpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.odmworker.Entity.ODMJobs;
import com.odmworker.Repository.JobRepository;
import com.odmworker.Service.IJobScheduleCreator;
import com.odmworker.Service.IJobService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import com.odmworker.JobTask.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.*;
import org.springframework.scheduling.quartz.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author ADMIN
 */
@Slf4j
@Service
public class JobServiceImpl implements IJobService {

    private static final String jobTask = "com.odmworker.JobTask.JobTask",
            jobFind = "com.odmworker.JobTask.JobFind",
            jobMerge = "com.odmworker.JobTask.JobMerge",
            jobMail = "com.odmworker.JobTask.JobEmail";

    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private IJobScheduleCreator scheduleCreator;

    @Override
    public List<ODMJobs> getODMJob() {
        return jobRepository.findByQueue(false);
    }

    public void setQueueJob() {
        List<ODMJobs> jobs = new ArrayList();
        ODMJobs newJob = new ODMJobs();;
        jobs = this.getODMJob();
        for (int i = 0; i <= jobs.size(); i++) {
            newJob = jobs.get(i);
            newJob.setInQueue(true);
            jobRepository.save(newJob);
            System.out.println("Add new Job: " + newJob.getName() + " to Queue success!");
            newJob = null;
        }

    }

    @Override
    public ODMJobs getJobById(int id) {
        return jobRepository.findById(id).get();
    }

    @Override
    public void startAllSchedulers() {
        List<ODMJobs> jobInfoList = jobRepository.findAll();
        if (jobInfoList != null) {
            Scheduler scheduler = schedulerFactoryBean.getScheduler();

            jobInfoList.forEach(jobInfo -> {
                try {
                    JobDetail jobDetail = JobBuilder.newJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()))
                            .withIdentity(jobInfo.getName(), jobInfo.getJobGroup()).build();
                    if (!scheduler.checkExists(jobDetail.getKey())) {
                        Trigger trigger;
                        jobDetail = scheduleCreator.createJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()),
                                false, context, jobInfo.getName(), jobInfo.getJobGroup());

                        if (jobInfo.getCronJob() && CronExpression.isValidExpression(jobInfo.getCronExpression())) {
                            trigger = scheduleCreator.createCronTrigger(jobInfo.getName(), new Date(),
                                    jobInfo.getCronExpression(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
                        } else {
                            trigger = scheduleCreator.createSimpleTrigger(jobInfo.getName(), new Date(),
                                    jobInfo.getRepeatTime(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
                        }

                        scheduler.scheduleJob(jobDetail, trigger);

                    }
                } catch (ClassNotFoundException e) {
                    log.error("Class Not Found - {}", jobInfo.getJobClass(), e);
                } catch (SchedulerException e) {
                    log.error(e.getMessage(), e);
                }
            });
        }
    }

    @Override
    public void scheduleNewJob(ODMJobs jobInfo) throws Exception {
        if (jobRepository.findByNameAndGroup(jobInfo.getName(), jobInfo.getJobGroup()) != null) {
            throw new Exception("Duplicated Job");
        }
        try {
            Scheduler scheduler = schedulerFactoryBean.getScheduler();

            JobDetail jobDetail = JobBuilder.newJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()))
                    .withIdentity(jobInfo.getName(), jobInfo.getJobGroup()).build();
            if (!scheduler.checkExists(jobDetail.getKey())) {

                jobDetail = scheduleCreator.createJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()),
                        false, context, jobInfo.getName(), jobInfo.getJobGroup());

                Trigger trigger;
                if (jobInfo.getCronJob()) {
                    trigger = scheduleCreator.createCronTrigger(jobInfo.getName(), new Date(), jobInfo.getCronExpression(),
                            SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
                } else {
                    trigger = scheduleCreator.createSimpleTrigger(jobInfo.getName(), new Date(), jobInfo.getRepeatTime(),
                            SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
                }

                scheduler.scheduleJob(jobDetail, trigger);
                jobRepository.save(jobInfo);
            } else {
                log.error("scheduleNewJobRequest.jobAlreadyExist");
            }
        } catch (ClassNotFoundException e) {
            log.error("Class Not Found - {}", jobInfo.getJobClass(), e);
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void updateScheduleJob(ODMJobs jobInfo) {
        ODMJobs odmjob = jobRepository.findByName(jobInfo.getName());
        Trigger newTrigger;
        if (jobInfo.getCronJob()) {
            newTrigger = scheduleCreator.createCronTrigger(odmjob.getName(), new Date(), odmjob.getCronExpression(),
                    SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
        } else {
            newTrigger = scheduleCreator.createSimpleTrigger(odmjob.getName(), new Date(), odmjob.getRepeatTime(),
                    SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
        }
        try {
            schedulerFactoryBean.getScheduler().rescheduleJob(TriggerKey.triggerKey(odmjob.getName()), newTrigger);
            jobRepository.save(odmjob);
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean unScheduleJob(String jobName) {
        try {
            return schedulerFactoryBean.getScheduler().unscheduleJob(new TriggerKey(jobName));
        } catch (SchedulerException e) {
            log.error("Failed to un-schedule job - {}", jobName, e);
            return false;
        }
    }

    @Override
    public boolean deleteJob(ODMJobs jobInfo) {
        try {
            ODMJobs odmjob = jobRepository.findByName(jobInfo.getName());
            jobRepository.delete(odmjob);
            log.info("deleted " + jobInfo.getName() + " Success");
            return schedulerFactoryBean.getScheduler().deleteJob(new JobKey(jobInfo.getName(), jobInfo.getJobGroup()));
        } catch (SchedulerException e) {
            log.error("Failed to delete job - {}", jobInfo.getName(), e);
            return false;
        }
    }

    @Override
    public void deleteAllJob() throws SchedulerException {
        schedulerFactoryBean.getScheduler().clear();
    }

    @Override
    public boolean pauseJob(ODMJobs jobInfo) {
        try {
            //ODMJobs odmjob = jobRepository.findByName(jobInfo.getName());
            schedulerFactoryBean.getScheduler().pauseJob(JobKey.jobKey(jobInfo.getName(), jobInfo.getJobGroup()));
            log.info("paused " + jobInfo.getName() + " Success");
            return true;
        } catch (SchedulerException e) {
            log.error("Failed to pause job - {}", jobInfo.getName(), e);
            return false;
        }
    }

    @Override
    public boolean resumeJob(ODMJobs jobInfo) {
        try {
            ODMJobs odmjob = jobRepository.findByName(jobInfo.getName());
            schedulerFactoryBean.getScheduler().resumeJob(new JobKey(odmjob.getName(), odmjob.getJobGroup()));
            log.info("resumed " + jobInfo.getName() + " Success");
            return true;
        } catch (SchedulerException e) {
            log.error("Failed to resume job - {}", jobInfo.getName(), e);
            return false;
        }
    }

    @Override
    public void startJobNow(ODMJobs jobInfo) throws Exception {

        if (jobRepository.findByNameAndGroup(jobInfo.getName(), jobInfo.getJobGroup()) != null) {
            try {
                ODMJobs odmjob = jobRepository.findByNameAndGroup(jobInfo.getName(), jobInfo.getJobGroup());
                schedulerFactoryBean.getScheduler().triggerJob(new JobKey(odmjob.getName(), odmjob.getJobGroup()));
                log.info("started " + jobInfo.getName() + " Success");
            } catch (SchedulerException e) {
                log.error("Failed to start new job - {}", jobInfo.getName(), e);
            }
        } else {
            try {
                Scheduler scheduler = schedulerFactoryBean.getScheduler();

                JobDetail jobDetail = JobBuilder.newJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()))
                        .withIdentity(jobInfo.getName(), jobInfo.getJobGroup()).build();
                if (!scheduler.checkExists(jobDetail.getKey())) {

                    jobDetail = scheduleCreator.createJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()),
                           true, context, jobInfo.getName(), jobInfo.getJobGroup());
                    scheduler.addJob(jobDetail, true);
                    scheduler.triggerJob(new JobKey(jobInfo.getName(), jobInfo.getJobGroup()));
                    jobRepository.save(jobInfo);
                    log.info("started " + jobInfo.getName() + " Success");
                } else {
                    log.error("scheduleNewJobRequest.jobAlreadyExist");
                }
            } catch (ClassNotFoundException e) {
                log.error("Class Not Found - {}", jobInfo.getJobClass(), e);
            } catch (SchedulerException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void createNewJob(ODMJobs jobInfo) {
        try {
            jobInfo.setDone(false);
            jobInfo.setInQueue(false);
            jobInfo.setStart(false);
            jobInfo.setCronJob(false);
            jobInfo.setJobClass("JobTask");
            jobRepository.save(jobInfo);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

    @Override
    public ODMJobs findByNameAndGroup(String name, String jobGroup) throws Exception {
        return jobRepository.findByNameAndGroup(name, jobGroup);
    }

    

}
