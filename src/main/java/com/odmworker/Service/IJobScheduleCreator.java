/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Service;

import java.util.Date;
import org.quartz.*;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author ADMIN
 */
public interface IJobScheduleCreator {
    public JobDetail createJob(Class<? extends QuartzJobBean>jobClass, boolean isDurable,
                                ApplicationContext context,
                                String jobName, String jobGroup);
    public CronTrigger createCronTrigger(String triggerName,Date startDate,String cronExpression, int misFireInstruction);
    public SimpleTrigger createSimpleTrigger(String triggerName, Date startTime, Long repeatTime, int misFireInstruction);
}
