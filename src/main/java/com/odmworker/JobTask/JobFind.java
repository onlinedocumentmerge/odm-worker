/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.JobTask;

import com.odmworker.Service.IJobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@DisallowConcurrentExecution
public class JobFind extends QuartzJobBean{
    @Autowired
    private IJobService jobService;


    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
         jobService.setQueueJob();
        //System.out.println(e.getMessage());
        System.out.println("Waiting for new Job...");
    }

}
