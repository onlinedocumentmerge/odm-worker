package com.odmworker;

import com.odmworker.JobTask.JobFind;
import org.modelmapper.ModelMapper;
import com.odmworker.config.JmsConfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import com.odmworker.Utils.CronExxpressionMaker;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
@SpringBootApplication
public class WorkerApplication implements WebApplicationInitializer {
//
//    public static Scheduler scheduler;
//    private static JobKey jobKeyA = new JobKey("Find", "MasterGroup");
//    
//
//    public WorkerApplication() throws SchedulerException {
//
//            this.scheduler = new StdSchedulerFactory().getScheduler();
//    }

    public static void main(String[] args) throws SchedulerException {
        SpringApplication.run(WorkerApplication.class, args);
//        scheduler.start();
//        scheduler.clear();
////        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("FindTrigger", "MasterGroup")
////                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow().withIntervalInSeconds(5).repeatForever()).build();
////        JobDetail job = JobBuilder.newJob(JobFind.class)
////                .withIdentity(jobKeyA).build();
////
////        scheduler.scheduleJob(job, trigger);

    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
    @Bean
    public CronExxpressionMaker cronExpressionMaker(){
        return new CronExxpressionMaker();
    }

    @Override
  	public void onStartup(ServletContext servletContext) throws ServletException {
  		  AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
  	        ctx.register(JmsConfig.class);
  	        ctx.setServletContext(servletContext);

  	        ServletRegistration.Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(ctx));
  	        servlet.setLoadOnStartup(1);
  	        servlet.addMapping("/");
  		
  	}

}
