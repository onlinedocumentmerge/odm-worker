package com.odmworker.Entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ADMIN
 */
@Getter
@Setter
@Entity
@Table(name = "ODMJobs")
public class ODMJobs implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "jobGroup", nullable = false)
    private String jobGroup;
    @Column(name = "isDone", nullable = false)
    private boolean isDone;
    @Column(name = "isInQueue", nullable = false)
    private boolean inQueue;
    
    @Column(name = "isStart", nullable = false)
    private boolean isStart;
    private String jobClass;

    private String cronExpression;
    @Column(nullable = false)
    private Long repeatTime;
    @Column(nullable = false)
    private Boolean cronJob;
}
