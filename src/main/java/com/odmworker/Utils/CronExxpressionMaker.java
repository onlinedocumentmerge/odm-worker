/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Utils;

import java.text.*;
import java.time.LocalDateTime;
import java.util.*;
import org.quartz.CronExpression;

/**
 *
 * @author ADMIN
 */
public class CronExxpressionMaker {

     public String CronMaker(){
        LocalDateTime dateTime = LocalDateTime.now();
        //CronExpression cronExpression = new CronExpression();
        return toCron(
                String.valueOf(dateTime.getSecond()),
                String.valueOf(dateTime.getMinute()),
                String.valueOf(dateTime.getHour()),
                String.valueOf(dateTime.getDayOfMonth()),
                String.valueOf(dateTime.getMonthValue()),
                String.valueOf(dateTime.getYear()));        
    }


    public static String toCron(final String second,final String mins, final String hrs, final String dayOfMonth, final String month, final String year) {
        return String.format("%s %s %s %s %s ? %s",second, mins, hrs, dayOfMonth, month, year);
    }
}
