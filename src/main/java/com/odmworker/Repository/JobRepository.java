/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odmworker.Repository;

/**
 *
 * @author ADMIN
 */
import com.odmworker.Entity.ODMJobs;
import java.sql.ResultSet;
import org.springframework.data.repository.*;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<ODMJobs, Integer>{
    @Query(value = "SELECT * FROM ODMJobs oj WHERE oj.is_in_queue = ?1",nativeQuery = true)
    public List<ODMJobs> findByQueue( boolean isInQueue);
    @Query(value = "SELECT * FROM ODMJobs oj WHERE oj.name = ?1",nativeQuery = true)
    public ODMJobs findByName( String name);
    @Query(value = "SELECT * FROM ODMJobs oj WHERE oj.name = ?1 and oj.job_group = ?2",nativeQuery = true)
    public ODMJobs findByNameAndGroup( String name,String group);
}
